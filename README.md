# Shadow Repackaged

[[_TOC_]]

## Introduction

This is Shadow AppImages repackaged with some adjustments / tuning / improvements / guide to support more distributions (Ubuntu 21.10+, Manjaro, Arch, SteamDeck(Desktop Mode), Fedora...) !

## Content

 - AppRun removal: AppRun is legacy tech and has been removed) - https://docs.appimage.org/introduction/software-overview.html
 - Extra libs removal: This is part1 of cleaning process to ensure better compatibility with newer/"rolling release" distributions 
 - Possible improvements / bash script changes

## Known Issues

[See here](https://shadow-codex.com/shadow-linux-known-issues/)

## Debug

```bash
find ~/.* -type f -name shadow.log -o -name shadow-launcher.log
```

or to provide full logs at once :
```bash
find ~/.* -type f -name "shadow*.log" -print0 | xargs -0 tar czvf ~/shadow-logs.tgz
```

## Installation / Usage

It is used like official AppImage, shouldn't be run as **root** or **sudo** and may requires **--no-sandbox** parameter and that your are synchronized with NTP time servers or you may not be able to launch or authenticate on Shadow's SSO and add your local linux user to **input** group (`sudo usermod -aG input $USER`):

### Packages prerequisites

**Click on arrows bellow related to your distribution to unfold details**

<details>
<summary>Debian 11 / Devuan (Chimaera)</summary>

```bash
sudo apt install libxcb-randr0 libxcb-render0 libxcb-render-util0 libxcb-image0 libxcb-keysyms1 libxcb-xinput0 libxcb-cursor0 libva2 \
libva-drm2 libva-glx2 libva-x11-2 libva-wayland2 libinput10 libcurl4 libpulse0 libgtk-3-0 libasound2 libgbm1 libnspr4 libnss3 \
libvdpau1 libxss1 libdbus-glib-1-2 libdbusmenu-glib4 libdbusmenu-gtk4 libgles2 libsdl2-2.0-0 xdg-utils gnome-keyring
```
<details>
<summary>Intel Gen <8</summary>

starts shadow using : `LIBVA_DRIVER_NAME=i965 ./Shadow*.AppImage`
</details>

<details>
<summary>Intel Gen >8</summary>

Requires `non-free` enabled in `/etc/apt/sources.list` :
```bash
sudo apt install intel-media-va-driver-non-free
```

and/or starts Shadow using : `LIBVA_DRIVER_NAME=iHD ./Shadow*.AppImage`
</details>

<details>
<summary>Nvidia</summary>

Install Nvidia latest proprietary driver and select in "FFmpeg" decoder in Shadow Launcher Settings (BlackList "nouveau" driver if required)
</details>
</details>

<details>
<summary>Ubuntu LTS 18.04 / 20.04 / 22.04</summary>

```bash
sudo apt install libxcb-randr0 libxcb-render0 libxcb-render-util0 libxcb-image0 libxcb-keysyms1 libxcb-xinput0 libxcb-cursor0 libva2 \
libva-drm2 libva-glx2 libva-x11-2 libva-wayland2 libinput10 libcurl4 libpulse0 libgtk-3-0 libasound2 libgbm1 libnspr4 libnss3 libindicator7 \
libvdpau1 libxss1 libdbus-glib-1-2 libdbusmenu-glib4 libdbusmenu-gtk4 libgles2 libsdl2-2.0-0 xdg-utils gnome-keyring
```
<details>
<summary>Intel Gen <8</summary>


starts shadow using : `LIBVA_DRIVER_NAME=i965 ./Shadow*.AppImage`
</details>
<details>
<summary>Intel Gen >8</summary>

```bash
sudo apt install intel-media-va-driver-non-free
````

and/or starts Shadow using : `LIBVA_DRIVER_NAME=iHD ./Shadow*.AppImage`
</details>
<details>
<summary>AMD 22.04</summary>

Check : https://www.amd.com/en/support/kb/release-notes/rn-amdgpu-unified-linux-22-20
</details>
<details>
<summary>Nvidia</summary>

Install Nvidia latest proprietary driver and select in "FFmpeg" decoder in Shadow Launcher Settings (BlackList "nouveau" driver if required)
</details>
</details>

<details>
<summary>Fedora 35+</summary>

(Possibly broken with **June 7 2022 Shadow update** (could be link to curl version))

```bash
sudo dnf install xcb-util xcb-util-keysyms xcb-util-cursor SDL2 libva libva-utils libvdpau libinput mesa-libgbm libXdamage libXrandr libXcomposite nspr cups-libs libXdmcp libXi libbsd libXcursor libXtst atk alsa-lib nss libXScrnSaver at-spi2-atk gdk-pixbuf2 gtk3 gnome-keyring
```
<details>
<summary>Intel Media (Non-free)</summary>

```bash
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && sudo dnf install intel-media-driver
```
</details>
<details>
<summary>Nvidia</summary>

Install Nvidia latest proprietary driver and select in "FFmpeg" decoder in Shadow Launcher Settings (BlackList "nouveau" driver if required)
</details>
<details>
<summary>USB over IP</summary>

See this link : https://gitlab.com/aar642/shadow-appimage#shadowusb-installation-update-procedure
</details>
</details>

<details>
<summary>Arch / Manjaro / SteamDeck</summary>

```bash
sudo pacman -Sy libx11 libxext libva libbsd nss libvdpau atk libxss xcb-util-cursor xcb-util-keysyms libinput \
libcups libxi libxtst pango alsa-lib gtk2 gtk3 at-spi2-core libpulse sdl2 dbus-glib libindicator-gtk2 \
libdbusmenu-gtk2 gnome-keyring
````
<details>
<summary>Intel Media</summary>

```bash
sudo pacman -Sy intel-media-driver
```
</details>
<details>
<summary>Nvidia</summary>

Install Nvidia latest proprietary driver and select in "FFmpeg" decoder in Shadow Launcher Settings (BlackList "nouveau" driver if required)
</details>
<details>
<summary>USB over IP</summary>

See this link : https://gitlab.com/aar642/shadow-appimage#shadowusb-installation-update-procedure
</details>
</details>

<details>
<summary>Gentoo (WIP - Not working)</summary>

add if missing `USE="X wayland"` to /etc/portage/make.conf 
```bash
sudo emerge --ask x11-libs/libxcb x11-libs/xcb-util-cursor x11-libs/xcb-util-keysyms media-libs/libpulse x11-libs/libva x11-libs/libvdpau \
x11-libs/libX11 media-libs/libpng dev-libs/libinput dev-libs/atk x11-libs/libXrandr x11-libs/libXcursor x11-libs/libva x11-libs/libX11 \
dev-libs/libdbusmenu x11-libs/libXcomposite x11-libs/libXi x11-libs/libxkbcommon x11-libs/pango x11-libs/gdk-pixbuf x11-libs/libXdamage \
x11-libs/libXtst dev-libs/libxslt app-accessibility/at-spi2-atk media-libs/alsa-lib x11-libs/gtk+:3
```
</details>

<details>
<summary>Guix (Credits to Levenson & the Guix/Nix friends)</summary>

check <https://gitlab.com/nonguix/nonguix/-/merge_requests/277>

```bash
guix shell -L . shadow-pc@prod
export LIBVA_DRIVER_NAME=i965 LIBVA_DRI3_DISABLE=true allow_rgb10_configs=false # depends on your system !
shadow-pc 
```

</details>

<details>
<summary>Alpine (WIP Shadowcker - Untested)</summary>

```bash
apk add make git docker xhost
rc-update add docker
/etc/init.d/docker start
git clone https://gitlab.com/aar642/shadowcker.git
cd shadowcker
make stable
xhost +
make start
```
</details>

### Shadow Repackaged

#### Stable
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow_PC-x86_64.AppImage?job=build" -O ShadowPC_Repackaged-x86_64.AppImage
chmod a+x ShadowPC_Repackaged-x86_64.AppImage
./ShadowPC_Repackaged-x86_64.AppImage
```

#### Beta
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow_PC_Beta-x86_64.AppImage?job=build" -O ShadowPC_Repackaged_Beta-x86_64.AppImage
chmod a+x ShadowPC_Repackaged_Beta-x86_64.AppImage
./ShadowPC_Repackaged_Beta-x86_64.AppImage
```

#### Alpha
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow_PC_Alpha-x86_64.AppImage?job=build" -O ShadowPC_Repackaged_Alpha-x86_64.AppImage
chmod a+x ShadowPC_Repackaged_Alpha-x86_64.AppImage
./ShadowPC_Repackaged_Alpha-x86_64.AppImage
```

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discord.com/invite/shadowfr)
- [Discord Shadow EN](https://discord.gg/shadow)
- [Discord Shadow DE](https://discord.com/invite/ShadowDE)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

## Disclaimer

This is a community project, project is not affliated to Shadow in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Shadow](http://www.shadow.tech/).

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge
